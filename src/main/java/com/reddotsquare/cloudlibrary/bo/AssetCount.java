package com.reddotsquare.cloudlibrary.bo;

import java.util.ArrayList;
import java.util.List;

import com.reddotsquare.cloudlibrary.pojo.Asset;

/**
 *  This class holds total amount of assets which match
 * search criteria as well, as their count. 
 * 
 * @author dnefedchenko
 *
 */
public class AssetCount {
    private List<Asset> assets = new ArrayList<>();
    private Long count;

    public AssetCount(List<Asset> assets, Long count) {
        this.assets = assets;
        this.count = count;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AssetCount [assets=" + assets + ", count=" + count + "]";
    }
}
