package com.reddotsquare.cloudlibrary.bo;

import java.util.Date;

import com.reddotsquare.cloudlibrary.pojo.MatchCondition;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;


/**
 *  Business object holding multiple search criteria.
 * 
 * @author dnefedchenko
 *
 */
public class SearchBO {
    private String searchProperty;
    private String searchValue;
    private RdfClass rdfClass;
    private MatchCondition matcher;
    private Date fromDate;
    private Date toDate;
    private SortDirection sortDirection;
    private Integer pageNumber;
    private Integer limit;

    public String getSearchProperty() {
        return searchProperty;
    }

    public void setSearchProperty(String searchProperty) {
        this.searchProperty = searchProperty;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public SearchBO() {
        matcher = MatchCondition.CONTAINING;
        rdfClass = RdfClass.PROJECT;
        sortDirection = SortDirection.ASC;
    }

    public RdfClass getRdfClass() {
        return rdfClass;
    }

    public void setRdfClass(RdfClass rdfClass) {
        this.rdfClass = rdfClass;
    }

    public MatchCondition getMatcher() {
        return matcher;
    }

    public void setMatcher(MatchCondition matcher) {
        this.matcher = matcher;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "SearchBO [searchProperty=" + searchProperty + ", searchValue="
                + searchValue + ", rdfClass=" + rdfClass + ", fromDate="
                + fromDate + ", toDate=" + toDate + ", sortDirection="
                + sortDirection + ", pageNumber=" + pageNumber + ", limit="
                + limit + "]";
    }
}
