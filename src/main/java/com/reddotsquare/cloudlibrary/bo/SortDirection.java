package com.reddotsquare.cloudlibrary.bo;

public enum SortDirection {
    ASC, DESC
}
