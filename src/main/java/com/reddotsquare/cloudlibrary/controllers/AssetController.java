package com.reddotsquare.cloudlibrary.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.services.AssetService;

@Controller
@RequestMapping(value = "/assets")
public class AssetController {
    @Autowired private AssetService assetService;

    @RequestMapping(value = "/{rdfClass}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Asset create(@PathVariable RdfClass rdfClass, HttpServletRequest request) throws Exception {
        return assetService.save(request.getInputStream(), rdfClass);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Asset findById(@PathVariable String id) {
        return assetService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<Asset> findAll() {
        return assetService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) {
        assetService.delete(id);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public @ResponseBody Long count(@RequestParam RdfClass rdfClass) {
        return assetService.count(rdfClass);
    }
}
