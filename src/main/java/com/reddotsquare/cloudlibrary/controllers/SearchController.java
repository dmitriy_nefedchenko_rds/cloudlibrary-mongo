package com.reddotsquare.cloudlibrary.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reddotsquare.cloudlibrary.bo.AssetCount;
import com.reddotsquare.cloudlibrary.bo.SearchBO;
import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.services.SearchService;

@Controller
@RequestMapping(value = "/assets/search")
public class SearchController {
    @Autowired private SearchService searchService;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody AssetCount criteriaSearch(@RequestBody SearchBO searchBO) {
        return searchService.criteriaSearch(searchBO);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<Asset> findByRdfClass(@RequestParam RdfClass rdfClass, @RequestParam Integer limit) {
        return searchService.findByRdfClass(rdfClass, limit);
    }
}
