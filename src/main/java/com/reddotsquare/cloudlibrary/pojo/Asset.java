package com.reddotsquare.cloudlibrary.pojo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  Base class for any asset type in application.
 * 
 * @author dnefedchenko
 *
 */
@Document(collection="assets")
@CompoundIndexes({
    @CompoundIndex(name = "rdfClass_1_displayName_1_creationDate_1", def = "{'rdfClass': 1, 'displayName': 1, 'creationDate': 1}")
})
public class Asset {
    @Id protected String id;
    private RdfClass rdfClass;
    private String displayName;
    private String description;

    private Date creationDate;
    private Date modificationDate;

    public Asset() {
        this.creationDate = new Date();
        this.modificationDate = new Date();
    }

    public RdfClass getRdfClass() {
        return rdfClass;
    }
    public void setRdfClass(RdfClass rdfClass) {
        this.rdfClass = rdfClass;
    }
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    public Date getModificationDate() {
        return modificationDate;
    }
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
}
