package com.reddotsquare.cloudlibrary.pojo;

public enum AssetStatus {
    CURRENT,
    OBSOLETE,
    DEPRECATED
}
