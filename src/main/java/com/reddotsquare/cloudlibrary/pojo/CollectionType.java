package com.reddotsquare.cloudlibrary.pojo;

public enum CollectionType {
    CONTENT_BUNDLE,
    PRODUCT_LIBRARY,
    STORE_CONTENT,
    PROJECT_COLLECTION,
    USER_GROUP
}
