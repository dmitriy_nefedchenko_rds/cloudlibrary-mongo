package com.reddotsquare.cloudlibrary.pojo;

public enum MatchCondition {
    CONTAINING,
    NOT_CONTAINING,
    EQUAL_TO,
    NOT_EQUAL_TO;
}
