package com.reddotsquare.cloudlibrary.pojo;

public enum ProjectKind {
    CLIENT_BASE_PROJECT,
    CLIENT_PROJECT,
    VENDOR_BASE_PROJECT
}
