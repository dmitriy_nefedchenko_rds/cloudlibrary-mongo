package com.reddotsquare.cloudlibrary.pojo;

/**
 *  Enumeration that lists all available rdf classes.
 * 
 * @author dnefedchenko
 *
 */
public enum RdfClass {
    PROJECT,
    COLLECTION,
    MENU_CATALOGUE,
    DATA_VIZ,
    SCENE_DESCRIPTION,
    CONFIGURATION,
    APPLICATION,
    PLANOGRAM,
    SCENERY,
    IMAGE,
    PRODUCT
}
