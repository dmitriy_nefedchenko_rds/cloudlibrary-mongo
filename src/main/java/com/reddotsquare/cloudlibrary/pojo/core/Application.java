package com.reddotsquare.cloudlibrary.pojo.core;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Application extends Asset {
    public Application() {
        setRdfClass(RdfClass.APPLICATION);
    }

    public Application(String name, String description) {
        setDisplayName(name);
        setDescription(description);
    }

    @Override
    public String toString() {
        return "Application [getRdfClass()=" + getRdfClass()
                + ", getDisplayName()=" + getDisplayName()
                + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
