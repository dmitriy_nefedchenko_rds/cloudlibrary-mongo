package com.reddotsquare.cloudlibrary.pojo.core;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.CollectionType;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Collection extends Asset {
    public Collection() {
        setRdfClass(RdfClass.COLLECTION);
    }

    public Collection(String name, String description) {
        setDisplayName(name);
        setDisplayName(description);
    }

    private CollectionType type;

    public CollectionType getType() {
        return type;
    }

    public void setType(CollectionType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Collection [type=" + type + ", getRdfClass()=" + getRdfClass()
                + ", getDisplayName()=" + getDisplayName()
                + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
