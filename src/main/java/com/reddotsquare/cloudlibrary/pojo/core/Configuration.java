package com.reddotsquare.cloudlibrary.pojo.core;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

public class Configuration extends Asset {
    public Configuration() {
        setRdfClass(RdfClass.CONFIGURATION);
    }

    public Configuration(String name, String description) {
        setDisplayName(name);
        setDescription(description);
    }
}
