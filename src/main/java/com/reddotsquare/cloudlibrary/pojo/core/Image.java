package com.reddotsquare.cloudlibrary.pojo.core;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Image extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;
    private String filename;
    private Integer pixelHeight;
    private Integer pixelWidth;

    public Image() {
        setRdfClass(RdfClass.IMAGE);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getPixelHeight() {
        return pixelHeight;
    }

    public void setPixelHeight(Integer pixelHeight) {
        this.pixelHeight = pixelHeight;
    }

    public Integer getPixelWidth() {
        return pixelWidth;
    }

    public void setPixelWidth(Integer pixelWidth) {
        this.pixelWidth = pixelWidth;
    }

    @Override
    public String toString() {
        return "Image [variant=" + variant + ", version=" + version
                + ", status=" + status + ", filename=" + filename
                + ", pixelHeight=" + pixelHeight + ", pixelWidth=" + pixelWidth
                + ", getRdfClass()=" + getRdfClass() + ", getDisplayName()="
                + getDisplayName() + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
