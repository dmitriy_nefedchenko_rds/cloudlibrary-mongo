package com.reddotsquare.cloudlibrary.pojo.data;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class DataViz extends Asset {
    public DataViz() {
        setRdfClass(RdfClass.DATA_VIZ);
    }

    public DataViz(String name, String description) {
        setDisplayName(name);
        setDescription(description);
    }
}
