package com.reddotsquare.cloudlibrary.pojo.data;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Product extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;
    private String width;
    private String depth;
    private String height;
    private String gtin8;
    private String gtin12;
    private String gtin13;
    private String gtin14;
    private String upca;
    private String upcb;

    public Product() {
        setRdfClass(RdfClass.PRODUCT);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public String getGtin8() {
        return gtin8;
    }

    public void setGtin8(String gtin8) {
        this.gtin8 = gtin8;
    }

    public String getGtin12() {
        return gtin12;
    }

    public void setGtin12(String gtin12) {
        this.gtin12 = gtin12;
    }

    public String getGtin13() {
        return gtin13;
    }

    public void setGtin13(String gtin13) {
        this.gtin13 = gtin13;
    }

    public String getGtin14() {
        return gtin14;
    }

    public void setGtin14(String gtin14) {
        this.gtin14 = gtin14;
    }

    public String getUpca() {
        return upca;
    }

    public void setUpca(String upca) {
        this.upca = upca;
    }

    public String getUpcb() {
        return upcb;
    }

    public void setUpcb(String upcb) {
        this.upcb = upcb;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Product [variant=" + variant + ", version=" + version
                + ", status=" + status + ", width=" + width + ", depth="
                + depth + ", height=" + height + ", gtin8=" + gtin8
                + ", gtin12=" + gtin12 + ", gtin13=" + gtin13 + ", gtin14="
                + gtin14 + ", upca=" + upca + ", upcb=" + upcb
                + ", getRdfClass()=" + getRdfClass() + ", getDisplayName()="
                + getDisplayName() + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
