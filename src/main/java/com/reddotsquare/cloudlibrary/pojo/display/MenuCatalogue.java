package com.reddotsquare.cloudlibrary.pojo.display;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.pojo.core.Image;
import com.reddotsquare.cloudlibrary.pojo.data.Product;
import com.reddotsquare.cloudlibrary.pojo.scene.Planogram;
import com.reddotsquare.cloudlibrary.pojo.store.Scenery;

@Document(collection="assets")
public class MenuCatalogue extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;

    @DBRef(lazy = true)
    private List<Planogram> planogramSet;

    @DBRef(lazy = true)
    private List<Scenery> scenerySet;

    @DBRef(lazy = true)
    private List<Image> imageSet;

    @DBRef(lazy = true)
    private List<Product> productSet;

    public MenuCatalogue() {
        setRdfClass(RdfClass.MENU_CATALOGUE);
    }

    public MenuCatalogue(String name, String description) {
        setDisplayName(name);
        setDescription(description);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public List<Planogram> getPlanogramSet() {
        return planogramSet;
    }

    public void setPlanogramSet(List<Planogram> planogramSet) {
        this.planogramSet = planogramSet;
    }

    public List<Scenery> getScenerySet() {
        return scenerySet;
    }

    public void setScenerySet(List<Scenery> scenerySet) {
        this.scenerySet = scenerySet;
    }

    public List<Image> getImageSet() {
        return imageSet;
    }

    public void setImageSet(List<Image> imageSet) {
        this.imageSet = imageSet;
    }

    public List<Product> getProductSet() {
        return productSet;
    }

    public void setProductSet(List<Product> productSet) {
        this.productSet = productSet;
    }

    @Override
    public String toString() {
        return "MenuCatalogue [variant=" + variant + ", version=" + version
                + ", status=" + status + ", getRdfClass()=" + getRdfClass()
                + ", getDisplayName()=" + getDisplayName()
                + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
