package com.reddotsquare.cloudlibrary.pojo.display;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.ProjectKind;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.pojo.core.Application;
import com.reddotsquare.cloudlibrary.pojo.core.Collection;
import com.reddotsquare.cloudlibrary.pojo.core.Configuration;
import com.reddotsquare.cloudlibrary.pojo.data.DataViz;
import com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription;

@Document(collection="assets")
public class Project extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;
    private ProjectKind projectKind;

    @DBRef
    private MenuCatalogue menuCatalogue;

    @DBRef(lazy = true)
    private List<Collection> collectionSet;

    @DBRef(lazy = true)
    private List<DataViz> datavizSet;

    @DBRef
    private Application application;

    @DBRef
    private Configuration configuration;

    @DBRef
    private SceneDescription sceneDescription;

    public Project() {
        setRdfClass(RdfClass.PROJECT);
    }

    public ProjectKind getProjectKind() {
        return projectKind;
    }

    public void setProjectKind(ProjectKind projectKind) {
        this.projectKind = projectKind;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public MenuCatalogue getMenuCatalogue() {
        return menuCatalogue;
    }

    public void setMenuCatalogue(MenuCatalogue menuCatalogue) {
        this.menuCatalogue = menuCatalogue;
    }

    public List<Collection> getCollectionSet() {
        return collectionSet;
    }

    public void setCollectionSet(List<Collection> collectionSet) {
        this.collectionSet = collectionSet;
    }

    public List<DataViz> getDatavizSet() {
        return datavizSet;
    }

    public void setDatavizSet(List<DataViz> datavizSet) {
        this.datavizSet = datavizSet;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public SceneDescription getSceneDescription() {
        return sceneDescription;
    }

    public void setSceneDescription(SceneDescription sceneDescription) {
        this.sceneDescription = sceneDescription;
    }

    @Override
    public String toString() {
        return "Project [variant=" + variant + ", version=" + version
                + ", status=" + status + ", projectKind=" + projectKind
                + ", menuCatalogue=" + menuCatalogue + ", collectionSet="
                + collectionSet + ", datavizSet=" + datavizSet
                + ", application=" + application + ", configuration="
                + configuration + ", sceneDescription=" + sceneDescription
                + ", getRdfClass()=" + getRdfClass() + ", getDisplayName()="
                + getDisplayName() + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
