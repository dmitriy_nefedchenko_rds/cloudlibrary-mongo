package com.reddotsquare.cloudlibrary.pojo.scene;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Planogram extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;

    public Planogram() {
        setRdfClass(RdfClass.PLANOGRAM);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Planogram [variant=" + variant + ", version=" + version
                + ", status=" + status + ", getRdfClass()=" + getRdfClass()
                + ", getDisplayName()=" + getDisplayName()
                + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
