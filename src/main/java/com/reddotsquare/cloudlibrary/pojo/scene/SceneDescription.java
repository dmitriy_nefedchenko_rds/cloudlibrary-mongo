package com.reddotsquare.cloudlibrary.pojo.scene;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class SceneDescription extends Asset {
    public SceneDescription() {
        setRdfClass(RdfClass.SCENE_DESCRIPTION);
    }

    public SceneDescription(String name, String description) {
        this();
        setDisplayName(name);
        setDescription(description);
    }
}
