package com.reddotsquare.cloudlibrary.pojo.store;

import org.springframework.data.mongodb.core.mapping.Document;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.AssetStatus;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

@Document(collection = "assets")
public class Scenery extends Asset {
    private String variant;
    private String version;
    private AssetStatus status;

    public Scenery() {
        setRdfClass(RdfClass.SCENERY);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Scenery [variant=" + variant + ", version=" + version
                + ", status=" + status + ", getRdfClass()=" + getRdfClass()
                + ", getDisplayName()=" + getDisplayName()
                + ", getDescription()=" + getDescription()
                + ", getCreationDate()=" + getCreationDate()
                + ", getModificationDate()=" + getModificationDate()
                + ", getId()=" + getId() + "]";
    }
}
