package com.reddotsquare.cloudlibrary.repositories;

import org.springframework.data.repository.CrudRepository;

import com.reddotsquare.cloudlibrary.pojo.Asset;

/**
 *  General CRUD repository for the assets of a different types.
 * 
 * @author dnefedchenko
 *
 */
public interface AssetRepository extends CrudRepository<Asset, String>, CloudlibraryCustomRepository {
    
}
