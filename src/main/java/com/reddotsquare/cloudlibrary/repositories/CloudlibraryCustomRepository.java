package com.reddotsquare.cloudlibrary.repositories;

import java.util.List;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

/**
 *  Base repository for custom query implementation.
 * 
 * @author dnefedchenko
 *
 */
public interface CloudlibraryCustomRepository {
    public Asset findById(String id);
    public List<Asset> findAll();
    public List<Asset> findByRdfClass(RdfClass rdfClass, Integer limit);
}
