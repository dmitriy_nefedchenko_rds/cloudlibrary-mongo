package com.reddotsquare.cloudlibrary.repositories;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

public class CloudlibraryRepository implements CloudlibraryCustomRepository {
    @Autowired private MongoOperations mongoTemplate;

    @Override
    public Asset findById(String id) {
        return mongoTemplate.findById(id, Asset.class);
    }

    @Override
    public List<Asset> findByRdfClass(RdfClass rdfClass, Integer limit) {
        return mongoTemplate.find(query(where("rdfClass").is(rdfClass)), Asset.class);
    }

    @Override
    public List<Asset> findAll() {
        return mongoTemplate.findAll(Asset.class);
    }
}
