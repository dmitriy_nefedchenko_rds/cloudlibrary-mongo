package com.reddotsquare.cloudlibrary.services;

import java.io.InputStream;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.pojo.core.Application;
import com.reddotsquare.cloudlibrary.pojo.core.Collection;
import com.reddotsquare.cloudlibrary.pojo.core.Configuration;
import com.reddotsquare.cloudlibrary.pojo.core.Image;
import com.reddotsquare.cloudlibrary.pojo.data.DataViz;
import com.reddotsquare.cloudlibrary.pojo.data.Product;
import com.reddotsquare.cloudlibrary.pojo.display.MenuCatalogue;
import com.reddotsquare.cloudlibrary.pojo.display.Project;
import com.reddotsquare.cloudlibrary.pojo.scene.Planogram;
import com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription;
import com.reddotsquare.cloudlibrary.pojo.store.Scenery;

@Component
public class AssetMapper {
    private ObjectMapper mapper;

    public AssetMapper() {
        mapper = new ObjectMapper();
    }

    public Asset getEntity(InputStream stream, RdfClass rdfClass) throws Exception {
        if (RdfClass.PROJECT == rdfClass) {
            return mapper.readValue(stream, Project.class);
        } else if (RdfClass.COLLECTION == rdfClass) {
            return mapper.readValue(stream, Collection.class);
        } else if (RdfClass.APPLICATION == rdfClass) {
            return mapper.readValue(stream, Application.class);
        } else if (RdfClass.CONFIGURATION == rdfClass) {
            return mapper.readValue(stream, Configuration.class);
        } else if (RdfClass.DATA_VIZ == rdfClass) {
            return mapper.readValue(stream, DataViz.class);
        } else if (RdfClass.SCENE_DESCRIPTION == rdfClass) {
            return mapper.readValue(stream, SceneDescription.class);
        } else if (RdfClass.MENU_CATALOGUE == rdfClass) {
            return mapper.readValue(stream, MenuCatalogue.class);
        } else if (RdfClass.PLANOGRAM == rdfClass) {
            return mapper.readValue(stream, Planogram.class);
        } else if (RdfClass.SCENERY == rdfClass) {
            return mapper.readValue(stream, Scenery.class);
        } else if (RdfClass.IMAGE == rdfClass) {
            return mapper.readValue(stream, Image.class);
        } else if (RdfClass.PRODUCT == rdfClass) {
            return mapper.readValue(stream, Product.class);
        }
        throw new Exception("Failed to map input stream into an entity because of unknown rdf class.");
    }
}
