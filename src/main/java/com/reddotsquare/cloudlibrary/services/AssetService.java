package com.reddotsquare.cloudlibrary.services;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.repositories.AssetRepository;

@Service
public class AssetService {
    @Autowired private MongoOperations mongoTemplate;
    @Autowired private AssetRepository assetRepository;
    @Autowired private AssetMapper assetMapper;

    public Asset save(InputStream stream, RdfClass rdfClass) throws Exception {
        return assetRepository.save(assetMapper.getEntity(stream, rdfClass));
    }

    public Asset save(Asset asset) {
        return assetRepository.save(asset);
    }

    public Asset findById(String id) {
        return assetRepository.findById(id);
    }

    public List<Asset> findAll() {
        return assetRepository.findAll();
    }

    public Asset update(Asset asset) {
        asset.setModificationDate(new Date());
        return assetRepository.save(asset);
    }

    public void delete(String id) {
        assetRepository.delete(id);
    }

    public void delete(Asset asset) {
        assetRepository.delete(asset);
    }

    public long count(RdfClass rdfClass) {
        return mongoTemplate.count(query(where("rdfClass").is(rdfClass)), Asset.class);
    }
}
