package com.reddotsquare.cloudlibrary.services;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.reddotsquare.cloudlibrary.bo.AssetCount;
import com.reddotsquare.cloudlibrary.bo.SearchBO;
import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.MatchCondition;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;
import com.reddotsquare.cloudlibrary.repositories.AssetRepository;

@Service
public class SearchService {
    private static final int defaultPageSize = 20;

    @Autowired private MongoOperations mongoTemplate;
    @Autowired private AssetRepository assetRepository;

    public AssetCount basicCriteriaSearch(SearchBO searchCriterias) {
        BasicDBObject queryObject = new BasicDBObject();
        queryByRdfClass(queryObject, searchCriterias);
        queryBySearchProperty(queryObject, searchCriterias);
        queryByCreationDate(queryObject, searchCriterias);
        BasicQuery basicQuery = new BasicQuery(queryObject);
        skipResults(basicQuery, searchCriterias);
        sortResults(basicQuery, searchCriterias);
        limitResults(basicQuery, searchCriterias);
        return new AssetCount(mongoTemplate.find(basicQuery, Asset.class), mongoTemplate.count(basicQuery, Asset.class));
    }

    private void queryByRdfClass(BasicDBObject queryObject, SearchBO searchCriterias) {
        RdfClass rdfClass = searchCriterias.getRdfClass();
        if (!Objects.equals(null, rdfClass)) {
            queryObject.append("rdfClass", rdfClass.name());
        }
    }

    private void queryBySearchProperty(BasicDBObject queryObject, SearchBO searchCriterias) {
        if (searchCriterias.getMatcher() == MatchCondition.CONTAINING) {
            queryObject.append(searchCriterias.getSearchProperty(), Pattern.compile(searchCriterias.getSearchValue(), Pattern.CASE_INSENSITIVE));
        } else if (searchCriterias.getMatcher() == MatchCondition.NOT_CONTAINING) {
            queryObject.append(searchCriterias.getSearchProperty(), new BasicDBObject("$not", Pattern.compile(searchCriterias.getSearchValue(), Pattern.CASE_INSENSITIVE)));
        } else if (searchCriterias.getMatcher() == MatchCondition.EQUAL_TO) {
            queryObject.append(searchCriterias.getSearchProperty(), searchCriterias.getSearchValue());
        } else if (searchCriterias.getMatcher() == MatchCondition.NOT_EQUAL_TO) {
            queryObject.append(searchCriterias.getSearchProperty(), new BasicDBObject("$ne", searchCriterias.getSearchValue()));
        }
    }

    private void queryByCreationDate(BasicDBObject queryObject, SearchBO searchCriterias) {
        Date from = searchCriterias.getFromDate();
        Date to = searchCriterias.getToDate();
        if (!Objects.equals(null, from) && !Objects.equals(null, to)) {
            queryObject.append("creationDate", new BasicDBObject("$gte", toDayBeginning(from)).append("$lte", toDayEnd(to)));
        } else if (!Objects.equals(null, from)) {
            queryObject.append("creationDate", new BasicDBObject("$gte", toDayBeginning(from)));
        } else if (!Objects.equals(null, to)) {
            queryObject.append("creationDate", new BasicDBObject("$lte", toDayEnd(to)));
        }
    }

    private void skipResults(BasicQuery basicQuery, SearchBO searchCriterias) {
        int pageNumber = searchCriterias.getPageNumber();
        int itemsPerPage = (Objects.equals(null, searchCriterias.getLimit())
                || Objects.equals(0, searchCriterias.getLimit())) 
                ? defaultPageSize: searchCriterias.getLimit();
        if (!Objects.equals(null, pageNumber)) {
            basicQuery.skip(--pageNumber * itemsPerPage);
        }
    }

    private void sortResults(BasicQuery basicQuery, SearchBO searchCriterias) {
        basicQuery.with(new Sort(Direction.valueOf(searchCriterias.getSortDirection().name()), searchCriterias.getSearchProperty()));
    }

    private void limitResults(BasicQuery basicQuery, SearchBO searchCriterias) {
        basicQuery.limit((Objects.equals(null, searchCriterias.getLimit()) || Objects.equals(0, searchCriterias.getLimit())) 
                ? defaultPageSize: searchCriterias.getLimit());
    }

    public AssetCount criteriaSearch(SearchBO searchCriterias) {
        List<Criteria> criteriaList = new ArrayList<Criteria>();
        addRdfClassCriteria(criteriaList, searchCriterias);
        addSearchPropertyCriteria(criteriaList, searchCriterias);
        addCreationDateCriteria(criteriaList, searchCriterias);
        Criteria andCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
        Query criteriaQuery = query(andCriteria);
        setSort(criteriaQuery, searchCriterias);
        skipResults(criteriaQuery, searchCriterias);
        setLimit(criteriaQuery, searchCriterias);
        return new AssetCount(mongoTemplate.find(criteriaQuery, Asset.class), mongoTemplate.count(criteriaQuery, Asset.class));
    }

    private void addRdfClassCriteria(List<Criteria> criteriaList, SearchBO searchCriterias) {
        if (!Objects.equals(null, searchCriterias.getRdfClass())) {
            criteriaList.add(where("rdfClass").is(searchCriterias.getRdfClass()));
        }
    }

    private void addSearchPropertyCriteria(List<Criteria> criteriaList, SearchBO searchCriterias) {
        if (searchCriterias.getMatcher() == MatchCondition.CONTAINING) {
            criteriaList.add(where(searchCriterias.getSearchProperty()).regex(searchCriterias.getSearchValue(), "i"));
        } else if (searchCriterias.getMatcher() == MatchCondition.NOT_CONTAINING) {
            criteriaList.add(where(searchCriterias.getSearchProperty()).not().regex(searchCriterias.getSearchValue(), "i"));
        } else if (searchCriterias.getMatcher() == MatchCondition.EQUAL_TO) {
            criteriaList.add(where(searchCriterias.getSearchProperty()).is(searchCriterias.getSearchValue()));
        } else if (searchCriterias.getMatcher() == MatchCondition.NOT_EQUAL_TO) {
            criteriaList.add(where(searchCriterias.getSearchProperty()).ne(searchCriterias.getSearchValue()));
        }
    }

    private void addCreationDateCriteria(List<Criteria> criteriaList, SearchBO searchCriterias) {
        if (!Objects.equals(null, searchCriterias.getFromDate())) {
            criteriaList.add(where("creationDate").gte(toDayBeginning(searchCriterias.getFromDate())));
        }
        if (!Objects.equals(null, searchCriterias.getToDate())) {
            criteriaList.add(where("creationDate").lte(toDayEnd(searchCriterias.getToDate())));
        }
    }

    private void setSort(Query query, SearchBO searchCriterias) {
        query.with(new Sort(Direction.valueOf(searchCriterias.getSortDirection().name()), searchCriterias.getSearchProperty()));
    }

    private void skipResults(Query query, SearchBO searchCriterias) {
        int pageNumber = searchCriterias.getPageNumber();
        int itemsPerPage = (Objects.equals(null, searchCriterias.getLimit())
                || Objects.equals(0, searchCriterias.getLimit())) 
                ? defaultPageSize: searchCriterias.getLimit();
        if (!Objects.equals(null, pageNumber)) {
            query.skip(--pageNumber * itemsPerPage);
        }
    }

    private void setLimit(Query query, SearchBO searchCriterias) {
        query.limit((Objects.equals(null, searchCriterias.getLimit()) || Objects.equals(0, searchCriterias.getLimit())) 
                ? defaultPageSize: searchCriterias.getLimit());
    }

    public List<Asset> findByRdfClass(RdfClass rdfClass, Integer limit) {
        return mongoTemplate.find(query(where("rdfClass").is(rdfClass))
                .limit(Objects.equals(null, limit) || Objects.equals(0, limit) ? defaultPageSize : limit), Asset.class);
    }

    private Date toDayBeginning(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getMinimum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getMinimum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getMinimum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getMinimum(Calendar.MILLISECOND));
        return calendar.getTime();
    }

    private Date toDayEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
        return calendar.getTime();
    }
}
