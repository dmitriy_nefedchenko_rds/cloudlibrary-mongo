package com.reddotsquare.cloudlibrary.utils;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.reddotsquare.cloudlibrary.pojo.RdfClass;

public class DbPopulator {
    private static String[] names = {"Coldplay", "RCHP", "Muse", "Heelsong United", "ONE DIRECTION", "ONEREPUBLIC", "IMAGINE DRAGONS", "FLORIDA GEORGIA LINE",
            "5 SECONDS OF SUMMER", "MAROON 5", "BASTILLE", "MAGIC!", "NICO AND VINZ"};
    
    private static String[] descriptions = {"Brothers Sisters", "Shiver", "Yellow", "Trouble", "Do not Panic", "In My Place", "The Scientist",
            "Clocks", "God Put a Smile upon Your Face", "Moses", "2000 Miles", "Speed of Sound", "Fix You",
            "Talk", "The Hardest Part", "What If", "White Shadows", "Violet Hill", "Viva la Vida", "Lovers in Japan",
            "Lost!", "Lhuna", "Life in Technicolor II", "Strawberry Swing", "Christmas Lights", "Every Teardrop Is a Waterfall",
            "In My Place", "Paradise", "Charlie Brown", "Princess of China", "Up with the Birds", "U.F.O.", "Hurts Like Heaven",
            "Up in Flames", "Atlas", "Magic", "Midnight", "A Sky Full of Stars", "True Love", "Ink", "Miracles"};
    
    public static void main(String[] args) throws UnknownHostException, ParseException {
        MongoClient client = new MongoClient();
        DB cloudlibrary = client.getDB("cloudlibrary");
        DBCollection assets = cloudlibrary.getCollection("assets");

        createOneHundredThousandAssets(RdfClass.APPLICATION, assets);
        createOneHundredThousandAssets(RdfClass.COLLECTION, assets);
        createOneHundredThousandAssets(RdfClass.CONFIGURATION, assets);
        createOneHundredThousandAssets(RdfClass.IMAGE, assets);
        createOneHundredThousandAssets(RdfClass.DATA_VIZ, assets);
        createOneHundredThousandAssets(RdfClass.PRODUCT, assets);
        createOneHundredThousandAssets(RdfClass.MENU_CATALOGUE, assets);
        createOneHundredThousandAssets(RdfClass.PLANOGRAM, assets);
        createOneHundredThousandAssets(RdfClass.SCENE_DESCRIPTION, assets);
        createOneHundredThousandAssets(RdfClass.SCENERY, assets);
    }

    private static void createOneHundredThousandAssets(RdfClass rdfClass, DBCollection assets) throws ParseException {
        for (int i = 0; i < 100000; i++) {
            BasicDBObject document = new BasicDBObject();
            document.put("displayName", names[new Random().nextInt(names.length - 1)]);
            document.put("description", descriptions[new Random().nextInt(descriptions.length - 1)]);
            document.put("creationDate", generateDate());
            document.put("modificationDate", generateDate());
            setRdfClassProperty(document, rdfClass);

            assets.insert(document);
        }
    }

    private static Date generateDate() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        Date dateFrom = dateFormat.parse("2012");
        long timestampFrom = dateFrom.getTime();
        Date dateTo = dateFormat.parse("2016");
        long timestampTo = dateTo.getTime();
        Random random = new Random();
        long timeRange = timestampTo - timestampFrom;
        long randomTimestamp = timestampFrom + (long) (random.nextDouble() * timeRange);
        return new Date(randomTimestamp);
    }

    private static void setRdfClassProperty(BasicDBObject document, RdfClass rdfClass) {
        if (RdfClass.APPLICATION == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.core.Application");
            document.put("rdfClass", RdfClass.APPLICATION.toString());
        } else if (RdfClass.COLLECTION == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.core.Collection");
            document.put("rdfClass", RdfClass.COLLECTION.toString());
        } else if (RdfClass.CONFIGURATION == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.core.Configuration");
            document.put("rdfClass", RdfClass.CONFIGURATION.toString());
        } else if (RdfClass.IMAGE == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.core.Image");
            document.put("rdfClass", RdfClass.IMAGE.toString());
        } else if (RdfClass.DATA_VIZ == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.data.DataViz");
            document.put("rdfClass", RdfClass.DATA_VIZ.toString());
        } else if (RdfClass.PRODUCT == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.data.Product");
            document.put("rdfClass", RdfClass.PRODUCT.toString());
        } else if (RdfClass.MENU_CATALOGUE == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.display.MenuCatalogue");
            document.put("rdfClass", RdfClass.MENU_CATALOGUE.toString());
        } else if (RdfClass.PLANOGRAM == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.scene.Planogram");
            document.put("rdfClass", RdfClass.PLANOGRAM.toString());
        } else if (RdfClass.SCENE_DESCRIPTION == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription");
            document.put("rdfClass", RdfClass.SCENE_DESCRIPTION.toString());
        } else if (RdfClass.SCENERY == rdfClass) {
            document.put("_class", "com.reddotsquare.cloudlibrary.pojo.scene.Scenery");
            document.put("rdfClass", RdfClass.SCENERY.toString());
        }
    }
}
