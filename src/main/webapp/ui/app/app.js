'use strict';

angular.module('cloudlibraryUi.controllers', ['ngRoute']);
angular.module('cloudlibraryUi.services', ['ngResource']);

angular.module('cloudlibraryUi', ['ui.bootstrap', 'toastr', 'cloudlibraryUi.controllers', 'cloudlibraryUi.services'])
    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/assets', {
                templateUrl: 'app/assets/asset.html',
                controller: 'AssetListController'
            })
            .when('/assets/:id', {
                templateUrl: 'app/assets/asset.html',
                controller: 'AssetEditController'
            })
            .when('/search', {
                templateUrl: 'app/assets/search.html',
                controller: 'AssetSearchController'
            })
            .otherwise({redirectTo: '/search'});

        $locationProvider.html5Mode(true);
    }])
    .controller('NavigationController', ['$scope', '$location', function($scope, $location) {
        $scope.urlContains = function(urlChunk) {
            return $location.path().includes(urlChunk);
        };
    }])
    .constant('rdfClasses', [
                             "PROJECT", "COLLECTION", "A-FRAME", "AISLE", "ANIMATION", "APPLICATION", "BAYSURROUND", "CAMERA_FLY_THROUGH",
                             "CAROUSEL", "CEILING", "CHECKOUT", "CHILLER", "CONFIGURATION", "COUNTER", "CYSTOM_FIXTURE", "CUSTOM_POINT_OF_SALE",
                             "DATA_VIZ", "DOCUMENT", "DUMP_BIN", "FIN", "FIXTURE_CONTENT", "FLOOR", "FLOOR_PLAN", "FLOOR_STICKER", "FREE_STANDING_DISPLAY_UNIT",
                             "FREEZER", "GANTRY", "GLORIFIER", "GONDOLA", "HEADER_BOARD", "HOT_SPOT", "IMAGE", "LEAFLET", "LEGAL_ENTITY", "LOCATION_MAPPING",
                             "MAGAZINE_RACK", "MENU_CATALOGUE", "MODEL_INFORMATION", "MOVIE", "PALLET", "PALLET_WRAP", "PILLAR", "PLANOGRAM",
                             "PLUGIN", "POSTER", "PRODUCE_RACK", "PRODUCT", "RETAILER", "SCENE_DESCRIPTION", "SCENERY", "SECURITY_SHROUD",
                             "SELL_DOWN", "SHELF_BARKER", "SHELF_STRIP", "SHELL", "SHIPPER", "SIGN", "SKY", "STORE", "SUPPLIER", "TABLE", "USER",
                             "WALL", "WOBBLER"
                             ])
    .constant('assetStatuses', [
                                    {name: "Current", value: "CURRENT"},
                                    {name: "Deprecated", value: "DEPRECATED"},
                                    {name: "Obsolete", value: "OBSOLETE"}
                                ])
    .constant('collectionTypes', [
                                    {name: "Custom contend bundle", value: "CONTENT_BUNDLE"},
                                    {name: "Licensed product library", value: "PRODUCT_LIBRARY"},
                                    {name: "Licensed store content", value: "STORE_CONTENT"},
                                    {name: "Project collection", value: "PROJECT_COLLECTION"},
                                    {name: "User group", value: "USER_GROUP"}
                                ])
    .constant('projectKinds', [
                                    {name: "Client base project", value: "CLIENT_BASE_PROJECT"},
                                    {name: "Client project", value: "CLIENT_PROJECT"},
                                    {name: "Vendor base project", value: "VENDOR_BASE_PROJECT"}
                                ]);
