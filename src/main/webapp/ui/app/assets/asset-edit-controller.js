angular.module('AssetEditController', ['$scope', '$routeParams', 'AssetService', function($scope, $routeParams, AssetService) {
    $scope.asset = AssetService.get($routeParams.id);
}]);