'use strict';

angular.module('cloudlibraryUi.controllers')
    .controller('AssetListController', ['$scope', '$http', '$location', 'rdfClasses', 'assetStatuses', 'toastr', '$modal', 'collectionTypes', 'projectKinds', 'SearchService',
                                        function($scope, $http, $location, rdfClasses, assetStatuses, toastr, $modal, collectionTypes, projectKinds, SearchService) {
        $scope.rdfClasses = rdfClasses;
        $scope.assetStatuses = assetStatuses;
        $scope.collectionTypes = collectionTypes;
        $scope.projectKinds = projectKinds;

        $scope.assetModel = {
            headerMessage: '',
            rdfClass: $scope.rdfClasses[0],
            template: '',
            currentSetType: undefined
        };

        $scope.asset = {};

        function pickTemplate(rdfClass) {
            if (rdfClass === 'PROJECT') {
                $scope.assetModel.headerMessage = 'Project';
                $scope.assetModel.template = 'app/assets/templates/project-template.html';
            } else if (rdfClass === 'COLLECTION') {
                $scope.assetModel.headerMessage = 'Collection';
                $scope.assetModel.template = 'app/assets/templates/collection-template.html';
            } else if (rdfClass === 'APPLICATION') {
                $scope.assetModel.headerMessage = 'Application';
                $scope.assetModel.template = 'app/assets/templates/application-template.html';
            } else if (rdfClass === 'CONFIGURATION') {
                $scope.assetModel.headerMessage = 'Configuration';
                $scope.assetModel.template = 'app/assets/templates/configuration-template.html';
            } else if (rdfClass === 'SCENE_DESCRIPTION') {
                $scope.assetModel.headerMessage = 'Scene Description';
                $scope.assetModel.template = 'app/assets/templates/scene-description-template.html';
            } else if (rdfClass === 'MENU_CATALOGUE') {
                $scope.assetModel.headerMessage = 'Menu Catalogue';
                $scope.assetModel.template = 'app/assets/templates/menu-catalogue-template.html';
            } else if (rdfClass === 'DATA_VIZ') {
                $scope.assetModel.headerMessage = 'Data Viz';
                $scope.assetModel.template = 'app/assets/templates/data-viz-template.html';
            } else if (rdfClass === 'PLANOGRAM') {
                $scope.assetModel.headerMessage = 'Planogram';
                $scope.assetModel.template = 'app/assets/templates/planogram-template.html';
            } else if (rdfClass === 'IMAGE') {
                $scope.assetModel.headerMessage = 'Image';
                $scope.assetModel.template = 'app/assets/templates/image-template.html';
            } else if (rdfClass === 'PRODUCT') {
                $scope.assetModel.headerMessage = 'Product';
                $scope.assetModel.template = 'app/assets/templates/product-template.html';
            } else if (rdfClass === 'SCENERY') {
                $scope.assetModel.headerMessage = 'Scenery';
                $scope.assetModel.template = 'app/assets/templates/scenery-template.html';
            }
        }

        $scope.$watch('assetModel.rdfClass', function(newRdfClass, oldRdfClass) {
            if (newRdfClass === undefined) {
                return;
            }
            pickTemplate(newRdfClass);
        });

        function clearForm() {
            $scope.asset = {};
        }

        $scope.save = function() {
            var requestOptions = {
                method: 'POST',
                url: '/cloudlibrary/rest/assets/'.concat($scope.assetModel.rdfClass),
                data: $scope.asset
            };
            $http(requestOptions).then(
                    function(response) {
                        toastr.success('Asset has been saved successfully', 'Success', {closeButton: true});
                        clearForm();
                    },
                    function(error) {
                        toastr.error('Failed to save an asset', 'Error', {closeButton: true});
                    });
        };

        function fetchAssets(rdfClass) {
            var requestOptions = {
                url: '/cloudlibrary/rest/assets/search',
                method: 'GET',
                params: {
                    rdfClass: rdfClass,
                    limit: 15
                }
            };
            return $http(requestOptions);
        }

        $scope.addSet = function(setType, multipleChoise) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/assets/templates/modal/add-set-template.html',
                controller: 'AddPlanogramController',
                size: 'lg',
                resolve: {
                    assets: function() {
                        $scope.assetModel.currentSetType = setType;
                        $scope.assetModel.multipleChoise = multipleChoise;
                        $scope.asset[setType] = multipleChoise ? [] : '';

                        if (setType === 'planogramSet') {
                            return fetchAssets('PLANOGRAM');
                        } else if (setType === 'scenerySet') {
                            return fetchAssets('SCENERY');
                        } else if (setType === 'imageSet') {
                            return fetchAssets('IMAGE');
                        } else if (setType === 'productSet') {
                            return fetchAssets('PRODUCT');
                        } else if (setType === 'menuCatalogue') {
                            return fetchAssets('MENU_CATALOGUE');
                        } else if (setType === 'collectionSet') {
                            return fetchAssets('COLLECTION');
                        } else if (setType === 'datavizSet') {
                            return fetchAssets('DATA_VIZ');
                        }  else if (setType === 'application') {
                            return fetchAssets('APPLICATION');
                        } else if (setType === 'configuration') {
                            return fetchAssets('CONFIGURATION');
                        } else if (setType === 'sceneDescription') {
                            return fetchAssets('SCENE_DESCRIPTION');
                        }
                    }
                }
            });

            modalInstance.result.then(function (assets) {
                angular.forEach(assets, function(item) {
                    if (item.selected) {
                        if ($scope.assetModel.multipleChoise) {
                            $scope.asset[$scope.assetModel.currentSetType].push(item);
                        } else {
                            $scope.asset[$scope.assetModel.currentSetType] = item;
                        }
                        delete item.selected;
                    }
                });
              });
        };

        $scope.isCollapsed = false;

        $scope.showSelection = function() {
            $scope.isCollapsed = !$scope.isCollapsed;
        };
    }]);