angular.module('cloudlibraryUi.controllers')
    .controller('AssetSearchController', ['$scope', '$http', 'AssetService', 'SearchService', 'rdfClasses', 'toastr',
                                          function($scope, $http, AssetService, SearchService, rdfClasses, toastr) {
        $scope.rdfClasses = rdfClasses;

        $scope.sortDirections = [{value: 'ASC'}, {value: 'DESC'}];

        $scope.limits = [{value: 10}, {value: 25}, {value: 50}, {value: 100}];

        $scope.searchProperties = [{name: 'Display name', value: 'displayName'}, 
                                 {name: 'Description', value: 'description'}, 
                                 {name: 'Identifier', value: 'id'}];

        $scope.matchValues = [{name: 'Containing', value: 'CONTAINING'}, 
                                {name: 'Not containing', value: 'NOT_CONTAINING'}, 
                                {name: 'Equals', value: 'EQUAL_TO'}, 
                                {name: 'Not equals', value: 'NOT_EQUAL_TO'}];

        $scope.datepickerSettings = {
            popup: 'dd-MMMM-yyyy',
            isFromOpened: false,
            isToOpened: false,
            minDate: new Date(),
            maxDate: '2020-06-22',
            dateOptions: {formatYear: 'yy', startingDay: 1}
        };

        $scope.searchBO = {
            searchProperty: $scope.searchProperties[0].value,
            searchValue: '',
            matcher: $scope.matchValues[2].value,
            fromDate: new Date(1430769523999),
            toDate: new Date(1450769523999),
            rdfClass: 'PROJECT',
            sortDirection: $scope.sortDirections[0].value,
            limit: $scope.limits[0].value,
            pageNumber: 1
        };

        function fetchProjects(limit) {
            var requestOptions = {
                method: 'GET',
                url: '/cloudlibrary/rest/assets/search',
                params: {
                    rdfClass: 'PROJECT',
                    limit: limit
                }
            };
            return $http(requestOptions);
        }

        fetchProjects(10).then(function(response) {
            $scope.projects = response.data;
        });

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.open = function($event, datepicker) {
            datepicker === 'from' ? $scope.datepickerSettings.isFromOpened = true : $scope.datepickerSettings.isToOpened = true;
        };

        $scope.paginationSettings = {
            totalItems: undefined,
            maxSize: 5,
            boundaryLinks: true,
            rotate: false
        };

        $scope.calculateTotalPageAmount = function() {
            return Math.round($scope.paginationSettings.totalItems / $scope.searchBO.limit);
        };

        $scope.moreThanOnePageExists = function() {
            return $scope.paginationSettings.totalItems > $scope.searchBO.limit;
        };

        function updatePager(searchResult) {
            $scope.projects = searchResult.assets;
            $scope.paginationSettings.totalItems = searchResult.count;
        }

        $scope.search = function() {
            SearchService.search($scope.searchBO, function(searchResult) {
                updatePager(searchResult);
                if (searchResult.count === 0 ) {
                    toastr.info('There are no assets found', 'Info', {closeButton: true});
                }
            }, function(error) {
                toastr.error('Asset search failed', 'Error', {closeButton: true});
            });
        };
    }]);