'use strict';

angular.module('cloudlibraryUi.controllers')
    .controller('AddPlanogramController', ['$scope', '$modalInstance', 'assets', function($scope, $modalInstance, assets) {
        $scope.setModel = {
            assets: assets.data
        };

        $scope.ok = function () {
          $modalInstance.close($scope.setModel.assets);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
    }]);