angular.module('cloudlibraryUi.services')
    .factory('AssetService', ['$resource', function($resource) {
        return $resource('/cloudlibrary/rest/assets/:assetId', {assetId: '@id', limit: '@limit'});
    }]);