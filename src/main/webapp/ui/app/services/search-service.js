angular.module('cloudlibraryUi.services')
    .factory('SearchService', ['$resource', function($resource) {
        return $resource('/cloudlibrary/rest/assets/search', {},
                {
                    search: {
                        method: 'POST'
                    }
                });
    }]);