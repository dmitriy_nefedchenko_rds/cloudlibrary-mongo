package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.core.Application;

public class ApplicationMappingTest extends MappingTest {
    @Test
    public void shouldSaveApplicaion() {
        Application application = TestFixture.createDefaultApplication("Core application", "From cloudlbrary");

        save(application);

        Application found = (Application) findById(application.getId());

        assertNotNull(found);
        assertEquals(application.getId(), found.getId());
        assertEquals(application.getDisplayName(), application.getDisplayName());
        assertEquals(application.getDescription(), application.getDescription());
        assertEquals(application.getCreationDate(), application.getCreationDate());
        assertEquals(application.getModificationDate(), application.getModificationDate());
    }
}
