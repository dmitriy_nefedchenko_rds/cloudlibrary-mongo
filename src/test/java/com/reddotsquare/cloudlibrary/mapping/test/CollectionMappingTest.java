package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.core.Collection;

public class CollectionMappingTest extends MappingTest {
    @Test
    public void shouldSaveCollection() {
        Collection collection = TestFixture.createDefaultCollection("Default collection", "One more collection");
        save(collection);

        Collection found = (Collection) findById(collection.getId());

        assertNotNull(found);
        assertEquals(collection.getId(), found.getId());
        assertEquals(collection.getDisplayName(), found.getDisplayName());
        assertEquals(collection.getDescription(), found.getDescription());
        assertEquals(collection.getModificationDate(), found.getCreationDate());
        assertEquals(collection.getCreationDate(), found.getCreationDate());
    }
}
