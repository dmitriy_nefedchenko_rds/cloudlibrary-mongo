package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.core.Configuration;

public class ConfigurationMappingTest extends MappingTest {
    @Test
    public void shouldSaveConfiguration() {
        Configuration configuration = TestFixture.createDefaultConfiguration("Test configuration", "Default configuration");
        save(configuration);

        Configuration found = (Configuration) findById(configuration.getId());

        assertNotNull(found);
        assertEquals(configuration.getId(), found.getId());
        assertEquals(configuration.getDisplayName(), found.getDisplayName());
        assertEquals(configuration.getDescription(), found.getDescription());
        assertEquals(configuration.getCreationDate(), found.getCreationDate());
        assertEquals(configuration.getModificationDate(), found.getModificationDate());
    }
}
