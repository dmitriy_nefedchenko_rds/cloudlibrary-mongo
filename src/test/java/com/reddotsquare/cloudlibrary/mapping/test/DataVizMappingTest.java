package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.data.DataViz;

public class DataVizMappingTest extends MappingTest {
    @Test
    public void shouldSaveDataViz() {
        DataViz dataViz = TestFixture.createDefaultDataViz("Test data viz", "Data viz description");
        save(dataViz);

        DataViz found = (DataViz) findById(dataViz.getId());

        assertNotNull(found);
        assertEquals(dataViz.getId(), found.getId());
        assertEquals(dataViz.getDisplayName(), found.getDisplayName());
        assertEquals(dataViz.getDescription(), found.getDescription());
        assertEquals(dataViz.getCreationDate(), found.getCreationDate());
        assertEquals(dataViz.getModificationDate(), found.getModificationDate());
    }
}
