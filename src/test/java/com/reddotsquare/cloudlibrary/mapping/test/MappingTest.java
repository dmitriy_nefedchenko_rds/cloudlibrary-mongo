package com.reddotsquare.cloudlibrary.mapping.test;

import java.util.List;

import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.reddotsquare.cloudlibrary.pojo.Asset;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public abstract class MappingTest {
    @Autowired private MongoOperations mongoTemplate;

    protected void save(Asset entity) {
        mongoTemplate.save(entity);
    }

    protected Asset findById(String id) {
        return mongoTemplate.findById(id, Asset.class);
    }

    protected List<Asset> findAll() {
        return mongoTemplate.findAll(Asset.class);
    }

    protected void delete(Asset entity) {
        mongoTemplate.remove(entity);
    }

    @After
    public void tearDown() {
        mongoTemplate.dropCollection(Asset.class);
    }
}
