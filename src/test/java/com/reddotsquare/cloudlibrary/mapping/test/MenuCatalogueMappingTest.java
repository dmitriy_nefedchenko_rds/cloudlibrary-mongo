package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.display.MenuCatalogue;

public class MenuCatalogueMappingTest extends MappingTest {
    @Test
    public void shouldSaveMenuCatalogue() {
        MenuCatalogue menuCatalogue = TestFixture.createDefaultMenuCatalogue("Super menu catalogue", "O-la-la");

        save(menuCatalogue);

        MenuCatalogue found = (MenuCatalogue) findById(menuCatalogue.getId());

        assertNotNull(found);
        assertEquals(found.getId(), menuCatalogue.getId());
        assertEquals(found.getDisplayName(), menuCatalogue.getDisplayName());
        assertEquals(found.getDescription(), menuCatalogue.getDescription());
        assertEquals(found.getCreationDate(), menuCatalogue.getCreationDate());
        assertEquals(found.getModificationDate(), menuCatalogue.getModificationDate());
    }
}
