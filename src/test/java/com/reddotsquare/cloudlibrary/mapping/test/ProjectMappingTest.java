package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.core.Application;
import com.reddotsquare.cloudlibrary.pojo.core.Collection;
import com.reddotsquare.cloudlibrary.pojo.core.Configuration;
import com.reddotsquare.cloudlibrary.pojo.data.DataViz;
import com.reddotsquare.cloudlibrary.pojo.display.MenuCatalogue;
import com.reddotsquare.cloudlibrary.pojo.display.Project;
import com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription;

public class ProjectMappingTest extends MappingTest {
    @Test
    public void shouldSaveProject() {
        Project project = createProject();

        MenuCatalogue menuCatalogue = TestFixture.createDefaultMenuCatalogue("Project menu catalogue", "Menu catalogue description");
        project.setMenuCatalogue(menuCatalogue);

        Collection firstCollection = TestFixture.createDefaultCollection("First project colleciton", "with description");
        Collection secondCollection = TestFixture.createDefaultCollection("Second project colleciton", "with description");
        project.setCollectionSet((Arrays.asList(firstCollection, secondCollection)));

        DataViz firstDataViz = new DataViz("first data viz", "first description");
        DataViz secondDataViz = new DataViz("second data viz", "second description");
        project.setDatavizSet(Arrays.asList(firstDataViz, secondDataViz));

        Application application = TestFixture.createDefaultApplication("test application", "application description");
        project.setApplication(application);

        Configuration configuration = TestFixture.createDefaultConfiguration("default configuration", "test configuration");
        project.setConfiguration(configuration);

        SceneDescription sceneDescription = TestFixture.createDefaultSceneDescription("scene description", "description for scene description");
        project.setSceneDescription(sceneDescription);

        save(menuCatalogue);
        save(firstCollection);
        save(secondCollection);
        save(firstDataViz);
        save(secondDataViz);
        save(application);
        save(configuration);
        save(sceneDescription);

        save(project);

        Project found = (Project) findById(project.getId());

        assertNotNull(found);
        assertEquals(project.getId(), found.getId());
        assertEquals(project.getDisplayName(), found.getDisplayName());
        assertEquals(project.getDescription(), found.getDescription());
        assertEquals(project.getCreationDate(), found.getCreationDate());
        assertEquals(project.getModificationDate(), found.getModificationDate());

        MenuCatalogue projectMenuCatalogue = found.getMenuCatalogue();
        assertEquals(menuCatalogue.getId(), projectMenuCatalogue.getId());
        assertEquals(menuCatalogue.getDisplayName(), projectMenuCatalogue.getDisplayName());
        assertEquals(menuCatalogue.getDescription(), projectMenuCatalogue.getDescription());
        assertEquals(menuCatalogue.getCreationDate(), projectMenuCatalogue.getCreationDate());
        assertEquals(menuCatalogue.getModificationDate(), projectMenuCatalogue.getModificationDate());
    }

    private Project createProject() {
        Project project = new Project();
        project.setDisplayName("One more display name");
        project.setDescription("Project test description");
        return project;
    }
}
