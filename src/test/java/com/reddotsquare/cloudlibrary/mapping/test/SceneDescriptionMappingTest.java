package com.reddotsquare.cloudlibrary.mapping.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription;

public class SceneDescriptionMappingTest extends MappingTest {
    @Test
    public void shouldSaveSceneDescription() {
        SceneDescription sceneDescription = TestFixture.createDefaultSceneDescription("Test scene", "With description");
        save(sceneDescription);

        SceneDescription found = (SceneDescription) findById(sceneDescription.getId());

        assertNotNull(found);
        assertEquals(sceneDescription.getId(), found.getId());
        assertEquals(sceneDescription.getDisplayName(), found.getDisplayName());
        assertEquals(sceneDescription.getDescription(), found.getDescription());
        assertEquals(sceneDescription.getCreationDate(), found.getCreationDate());
        assertEquals(sceneDescription.getModificationDate(), found.getModificationDate());
    }
}
