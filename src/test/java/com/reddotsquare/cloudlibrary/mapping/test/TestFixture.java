package com.reddotsquare.cloudlibrary.mapping.test;

import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.core.Application;
import com.reddotsquare.cloudlibrary.pojo.core.Collection;
import com.reddotsquare.cloudlibrary.pojo.core.Configuration;
import com.reddotsquare.cloudlibrary.pojo.data.DataViz;
import com.reddotsquare.cloudlibrary.pojo.display.MenuCatalogue;
import com.reddotsquare.cloudlibrary.pojo.scene.SceneDescription;

public class TestFixture {
    public static Asset createDefault(String name, String description) {
        Asset asset = new Asset();
        asset.setDisplayName(name);
        asset.setDescription(description);
        return asset;
    }

    public static MenuCatalogue createDefaultMenuCatalogue(String name, String description) {
        return new MenuCatalogue(name, description);
    }

    public static Collection createDefaultCollection(String name, String description) {
        return new Collection(name, description);
    }

    public static Application createDefaultApplication(String name, String description) {
        return new Application(name, description);
    }

    public static DataViz createDefaultDataViz(String name, String description) {
        return new DataViz(name, description);
    }

    public static SceneDescription createDefaultSceneDescription(String name, String description) {
        return new SceneDescription(name, description);
    }

    public static Configuration createDefaultConfiguration(String name, String description) {
        return new Configuration(name, description);
    }
}
