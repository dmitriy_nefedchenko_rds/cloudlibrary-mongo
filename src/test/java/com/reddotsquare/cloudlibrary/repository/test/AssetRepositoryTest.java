package com.reddotsquare.cloudlibrary.repository.test;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.reddotsquare.cloudlibrary.mapping.test.TestFixture;
import com.reddotsquare.cloudlibrary.pojo.Asset;
import com.reddotsquare.cloudlibrary.pojo.display.Project;
import com.reddotsquare.cloudlibrary.repositories.AssetRepository;

/**
 * Unit test for {@link AssetRepository} 
 * 
 * @author dnefedchenko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class AssetRepositoryTest {
    @Autowired private AssetRepository testee;

    @Autowired private MongoOperations mongoTemplate;

    @Test
    public void testSave() {
        Asset created = testee.save(TestFixture.createDefault("Funny asset goes here", "And here is its description"));

        Asset found = mongoTemplate.findById(created.getId(), Project.class);

        Assert.assertNotNull(found);
        Assert.assertEquals(created.getId(), found.getId());
    }

    @Test
    public void testFindById() {
        Asset asset = TestFixture.createDefault("Funny asset goes here", "And here is its description");

        Asset found = testee.findById(asset.getId());

        Assert.assertNull(found);

        mongoTemplate.save(asset);

        found = testee.findById(asset.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(asset.getId(), found.getId());
    }

    @Test
    public void testFindByRdfClass() {
        Asset asset = TestFixture.createDefault("Funny asset goes here", "And here is its description");

        List<Asset> found = testee.findByRdfClass(asset.getRdfClass(), 1);

        Assert.assertTrue(found.isEmpty());

        mongoTemplate.save(asset);

        found = testee.findByRdfClass(asset.getRdfClass(), 1);
        Assert.assertFalse(found.isEmpty());
        Assert.assertTrue(found.size() == 1);
        Assert.assertEquals(asset.getId(), found.get(0).getId());
    }

    @Test
    public void testFindAll() {
        Asset asset = TestFixture.createDefault("Funny asset goes here", "And here is its description");

        List<Asset> found = testee.findAll();

        Assert.assertTrue(found.isEmpty());

        mongoTemplate.save(asset);

        found = testee.findAll();
        Assert.assertFalse(found.isEmpty());
        Assert.assertTrue(found.size() == 1);
        Assert.assertEquals(asset.getId(), found.get(0).getId());
    }

    @After
    public void tearDown() {
        mongoTemplate.dropCollection("assets");
    }
}
