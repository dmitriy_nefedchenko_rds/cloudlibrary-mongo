package com.reddotsquare.cloudlibrary.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:test-application-context.xml")
public class SpringMvcTestConfiguration {
    @Autowired private WebApplicationContext testContext;

    private MockMvc mvcMock;

    @Before
    public void setUp() {
        this.mvcMock = MockMvcBuilders.webAppContextSetup(this.testContext).build();
    }

    @Test
    public void testCreateProject() throws Exception {
        mvcMock.perform(post("/projects")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(getProjectJson()))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Test
    public void testFindProjectById() throws Exception {
        mvcMock.perform(post("/projects")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(getProjectJson()))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"));

        mvcMock.perform(get("/projects/1")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rdfClass").value("PROJECT"));
    }

    private String getProjectJson() {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("\"id\": \"1\",");
        jsonBuilder.append("\"rdfClass\": \"PROJECT\",");
        jsonBuilder.append("\"displayName\": \"One more project\",");
        jsonBuilder.append("\"description\": \"HO scale Athearn Union Pacific RR 50' gondola car train w/ load WEATHERED\",");
        jsonBuilder.append("\"projectKind\": \"custom\"");
        jsonBuilder.append("}");
        return jsonBuilder.toString();
    }
}
